const axios = jest.genMockFromModule('axios');

const res = {
    data: [
        {
            id: 1,
            name: 'paco'
        },
        {
            id: 2,
            name: 'maria'
        }
    ]
}

function get() {
    return new Promise((resolve, reject) => {
        resolve(res);
        reject('error');
    });
}

axios.get = get;
module.exports = axios;