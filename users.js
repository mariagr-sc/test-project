const axios = require('axios');

const getUsers = () => {
    return axios.get('https://jsonplaceholder.typicode.com/users')
    .then((data)=> {
        return data.data;
    });
};

module.exports = getUsers;