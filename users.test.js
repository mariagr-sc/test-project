const users =  require('./users');

jest.mock('axios');

it('There should return 2 users', () => {
    users().then((data) => {
        expect.assertions(1);
        return expect(data.length).toEqual(2);
    });
});

it('First username should be paco', () => {
    users().then(data => {
        expect.assertions(1);
        return expect(data[0].name).toEqual('paco');
    })
});